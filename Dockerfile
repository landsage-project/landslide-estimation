FROM python:latest
WORKDIR /code

COPY Map1.png Map1.png
COPY Map2.png Map2.png
COPY Map3.png Map3.png
COPY Map1TestFromApp.png Map1TestFromApp.png

ENV FLASK_APP=script3.py
ENV FLASK_RUN_HOST=0.0.0.0
RUN /usr/local/bin/python -m pip install --upgrade pip
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
EXPOSE 5000
COPY . .
CMD ["flask", "run"]