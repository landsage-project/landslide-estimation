# pip install Pillow
# pip install flask

from PIL import Image
import math
import flask
import urllib.request

from flask import Flask, request, jsonify
app = Flask(__name__)

# Define color codes for different risk levels
landslide_colors = {
    "ArtyClick Red": "very high",
    "ArtyClick Orange": "high",
    "ArtyClick Yellow": "moderate",
    "Citrus": "low",
    "Lawn Green": "very low"
}

soil_colors = {
    "ArtyClick Red": "Complex slope",
    "Science Blue": "Water",
    "International Orange": "Urban",
    "Pinkish Tan": "Silty clay loam",
    "Burly Wood": "Gravel clay loam",
    "Chalky": "Silty loam",
    "Golden Glow": "Gravel sandy loam",
    "Primrose": "Sandy loam",
    "Tan Green": "Clay loam",
    "Moss": "Loam",
}

slope_colors = {
    "Lawn Green": 0,
    "Kelly Green": 3.914,
    "Citrus": 7.828,
    "Bitter Lemon": 12.345,
    "ArtyClick Yellow": 16.561,
    "ArtyClick Amber": 20.776,
    "ArtyClick Orange": 25.293,
    "ArtyClick Warm Red": 30.111,
    "ArtyClick Red": 36.735
}

# Load input images
map1 = Image.open("Map1TestFromApp.png")
map2 = Image.open("Map2.png")
map3 = Image.open("Map3.png")

# print(map1.width)
# print(map1.height)

# Get pixel values for the given coordinates
def get_pixel_value(image, x, y):
    if x < 0 or x >= image.width or y < 0 or y >= image.height:
        # Handle out of bounds coordinates
        return None
    pixel_value = image.getpixel((x, y))
    # print(f"pixel_value = {pixel_value}")
    if not isinstance(pixel_value, tuple) or len(pixel_value) < 3 or len(pixel_value) > 4:
        # Handle invalid pixel values
        return None
    return pixel_value

# Get risk level for a given location on the landslide susceptibility map
def get_landslide_risk(x, y):
    color_code = get_pixel_value(map1, x, y)
    # print(f"get_landslide_risk color_code = {color_code}")
    nameColor = classify_color(color_code)
    if nameColor not in landslide_colors:
        return "Other"
    else:
        return landslide_colors[nameColor]

# Get soil type for a given location on the soil texture map
def get_soil_type(x, y):
    color_code = get_pixel_value(map2, x, y)
    # print(f"get_soil_type color_code = {color_code}")
    nameColor = classify_color(color_code)
    if nameColor not in soil_colors:
        return "Other"
    else:
        return soil_colors[nameColor]

# Get slope degree for a given location on the slope map
def get_slope_degree(x, y):
    color_code = get_pixel_value(map3, x, y)
    # print(f"get_slope_degree get_slope_degree = {color_code}")
    nameColor = classify_color(color_code)
    if nameColor not in slope_colors:
        return 0
    else:
        return slope_colors[nameColor]

# Check if the rainfall amount exceeds the threshold for the given risk level
def check_rainfall(rain_level, risk_level):
    if risk_level == "very high" and rain_level > 100:
        return True
    elif risk_level == "high" and rain_level > 150:
        return True
    elif risk_level == "moderate" and rain_level > 200:
        return True
    elif risk_level == "low" and rain_level > 250:
        return True
    elif risk_level == "very low" and rain_level > 300:
        return True
    else:
        return False

# Check if the given location is at risk of landslide
def check_landslide_risk(mapLandslide, x, y, rain_level):
    for i in range(x-1, x+2):
        for j in range(y-1, y+2):
            if get_pixel_value(mapLandslide, i, j) is not None:
                risk_level = get_landslide_risk(i, j)
                if check_rainfall(rain_level, risk_level):
                    soil_type = get_soil_type(i, j)
                    if soil_type == "Complex slope":
                        return True
    return False

# Check if the given location has slope greater than 30 degrees
def check_slope_risk(mapSlope, x, y):
    for i in range(x-1, x+2):
        for j in range(y-1, y+2):
            if get_pixel_value(mapSlope, i, j) is not None:
                slope_degree = get_slope_degree(i, j)
                if slope_degree > 30:
                    return True
    return False

def classify_color(rgb_color):
    color_range = {
        "Moss": [(0, 85, 0), (128, 255, 0)],
        "Tan Green": [(139, 139, 0), (205, 205, 102)],
        "Primrose": [(255, 200, 87), (255, 255, 153)],
        "Golden Glow": [(255, 217, 102), (255, 255, 153)],
        "Chalky": [(255, 224, 189), (255, 255, 230)],
        "Burly Wood": [(222, 184, 135), (255, 211, 155)],
        "Pinkish Tan": [(255, 192, 203), (255, 228, 225)],
        "International Orange": [(255, 79, 0), (255, 130, 0)],
        "Science Blue": [(0, 0, 204), (0, 204, 255)],
        "ArtyClick Red": [(230, 0, 0), (255, 51, 51)],
        "ArtyClick Orange": [(255, 85, 0), (255, 170, 85)],
        "ArtyClick Yellow": [(255, 255, 0), (255, 255, 170)],
        "Citrus": [(153, 255, 0), (255, 255, 153)],
        "Lawn Green": [(0, 128, 0), (0, 255, 0)],
        "Kelly Green": [(0, 128, 0), (76, 187, 23)],
        "Bitter Lemon": [(204, 255, 0), (255, 255, 153)],
        "ArtyClick Amber": [(255, 170, 0), (255, 204, 102)],
        "ArtyClick Warm Red": [(204, 0, 0), (255, 51, 51)]
    }
    
    for color_name, color_range in color_range.items():
        if color_range[0][0] <= rgb_color[0] <= color_range[1][0] and \
           color_range[0][1] <= rgb_color[1] <= color_range[1][1] and \
           color_range[0][2] <= rgb_color[2] <= color_range[1][2]:
            return color_name
    
    return "Other"

# def latlon_to_xy(lat, lon, width, height):
#     # Convert latitude and longitude to radians
#     lat_rad = math.radians(lat)
#     lon_rad = math.radians(lon)
    
#     # Set the Mercator projection scale
#     scale = 1 / math.cos(lat_rad)
    
#     # Calculate the x and y pixel coordinates
#     x = (lon_rad + math.pi) * (width / (2 * math.pi))
#     y = (height / 2) - (width * math.log(math.tan((math.pi / 4) + (lat_rad / 2))) / (2 * math.pi) * scale)
    
#     # Round the pixel coordinates to integers
#     x = int(round(x))
#     y = int(round(y))
    
#     return x, y


def latlon_to_xy(lat, lon, width, height, lat1, lon1, lat2, lon2):
    # Convert latitude and longitude to radians
    lat_rad = math.radians(lat)
    lon_rad = math.radians(lon)
    
    # Convert the corners of the image to radians
    lat1_rad = math.radians(lat1)
    lon1_rad = math.radians(lon1)
    lat2_rad = math.radians(lat2)
    lon2_rad = math.radians(lon2)
    
    # Calculate the width and height of the image in degrees
    delta_lat = lat2_rad - lat1_rad
    delta_lon = lon2_rad - lon1_rad
    
    # Calculate the x and y pixel coordinates
    x = int(round((lon_rad - lon1_rad) / delta_lon * width))
    y = int(round((lat2_rad - lat_rad) / delta_lat * height))
    
    return x, y

@app.route('/getLandslideRisk', methods = ['POST'])
def getLandslideRisk():
    if request.method == 'POST':

        # rain_data = {
        #     "rain_level": 1.2,
        #     "lat": 19.393637,
        #     "lon": 100.45059
        # }

        # list_rain_data = [
        #     {
        #         "rain_level": 100,
        #         "lat": 19.393637,
        #         "lon": 100.45059
        #     },
        #     {
        #         "rain_level": 200,
        #         "lat": 19.393637,
        #         "lon": 100.45059
        #     }
        # ]

        # urlMapLandslideSusceptibility = request.form.get('urlMapLandslideSusceptibility')
        # urlMapSoilTexture = request.form.get('urlMapSoilTexture')
        # urlMapSlope = request.form.get('urlMapSlope')

        # filename, headers = urllib.request.urlretrieve(urlMapLandslideSusceptibility)
        # imageMapLandslideSusceptibility = Image.open(filename)
        # filename, headers = urllib.request.urlretrieve(urlMapSoilTexture)
        # imageMapSoilTexture= Image.open(filename)
        # filename, headers = urllib.request.urlretrieve(urlMapSlope)
        # imageMapSlope= Image.open(filename)


        # print(f"x = {x}, y = {y}")

        # Example rainfall data

        # not risk
        # rain_data = {
        #     "rain_level": 200,
        #     "x": 3204,
        #     "y": 2563
        # }

        # risk
        # rain_data = {
        #     "rain_level": 200,
        #     "x": 467,
        #     "y": 2359
        # }

        # rain_level = rain_data["rain_level"]
        # x = rain_data["x"]
        # y = rain_data["y"]

        # Print the pixel coordinates for the given latitude and longitude
        # print(f"Pixel coordinates: ({x}, {y})")

        # Check if the given location and its 8 neighbors are at risk of landslide

        # if is_at_risk:
            # print("The location and its neighbors are at risk of landslide.")
        # else:
            # print("The location and its neighbors are not at risk of landslide.")

        # Check if the given location and its 8 neighbors have slope greater than 30 degrees

        # if has_steep_slope:
        #     print("The location and its neighbors have steep slope.")
        # else:
        #     print("The location and its neighbors do not have steep slope.")
        mapLandslide = map1
        mapSoil = map2
        mapSlope = map3

        latBottomLeftLandslide = 18.766996
        lonBottomLeftLandslide = 99.636653
        latTopRightLandslide = 19.778016
        lonTopRightLandslide = 100.676356

        data = request.get_json()  # get the JSON data from the request
        list_rain_data = data.get('list_rain_data') 

        # list_rain_data = request.form.get('list_rain_data')
        # list_rain_data = None
        # if 'list_rain_data' in request.form:
        #     list_rain_data = request.form.to_dict(flat=False)['list_rain_data']
        #     # process the list_rain_data parameter...
        # else:
        #     print("The list_rain_data parameter was not found in the request.")
        # print(list_rain_data)

        results = []
        if list_rain_data is not None:
            for rain_data in list_rain_data:
                rain_level = rain_data["rain_level"]
                latRain = rain_data['lat']
                lonRain = rain_data['lon']

                xLandslide, yLandslide = latlon_to_xy(latRain, lonRain, mapLandslide.width, mapLandslide.height, latBottomLeftLandslide, lonBottomLeftLandslide, latTopRightLandslide, lonTopRightLandslide)
                is_at_risk = check_landslide_risk(mapLandslide, xLandslide, yLandslide, rain_level)

                xSlope, ySlope = latlon_to_xy(latRain, lonRain, mapSlope.width, mapSlope.height, latBottomLeftLandslide, lonBottomLeftLandslide, latTopRightLandslide, lonTopRightLandslide)
                has_steep_slope = check_slope_risk(mapSlope, xSlope, ySlope)

                isRisk = False
                if is_at_risk and has_steep_slope :
                    isRisk = True
                
                rain_data["isRisk"] = isRisk
                results.append(rain_data)

        # print(results)
        response = flask.jsonify(results)

        response.headers.add('Access-Control-Allow-Origin', '*')
        response.headers.add("Access-Control-Allow-Methods", "POST")
        response.headers.add("Access-Control-Allow-Headers", "*")

        return response

if __name__ == "__main__":
    app.run()